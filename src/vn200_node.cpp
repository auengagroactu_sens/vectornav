/*
 * MIT License (MIT)
 *
 * Copyright (c) 2013 Dereck Wonnacott <dereck@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 *
 */


#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <stdlib.h>     /* exit, EXIT_FAILURE */

#include "vectornav.h"

#include <ros/ros.h>
#include <tf/tf.h>
#include <signal.h>
// Message Types
#include <vectornav/utc_time.h>
#include <vectornav/gps.h>
#include <vectornav/ins.h>
#include <vectornav/sensors.h>
#include <vectornav/serial_data.h>
#include <vectornav/vectornav_imu.h>

#include <vectornav/sync_in.h>
#include <ros/xmlrpc_manager.h>

#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <sensor_msgs/NavSatStatus.h>

// Signal-safe flag for whether shutdown is requested
sig_atomic_t volatile g_request_shutdown = 0;


// Params
std::string imu_frame_id, gps_frame_id;

// Publishers
ros::Publisher pub_ins;
ros::Publisher pub_gps;
ros::Publisher pub_sensors;
ros::Publisher pub_sync_in;

ros::Publisher pub_std_imu;
sensor_msgs::Imu msg_std_imu;

ros::Publisher pub_std_gps;
sensor_msgs::NavSatFix msg_std_gps;

ros::Publisher pub_vn_imu;
vectornav::vectornav_imu msg_vn_imu;

ros::Publisher pub_binary_data;
vectornav::serial_data msg_binary_serial;

// Device
Vn200 vn200;

int ins_seq           = 0;
int imu_seq           = 0;
int gps_seq           = 0;
int sync_in_seq       = 0;
int msg_cnt           = 0;
int last_group_number = 0;


std::string port;
char vn_error_msg[100];

unsigned int imu_resv_count = 1;

struct ins_binary_data_struct
{
    uint64_t gps_time;
    uint64_t sync_in_time;
    float yaw;
    float pitch;
    float roll;
    double latitude;
    double longitude;
    double altitude;
    float vel_north;
    float vel_east;
    float vel_down;
    uint16_t ins_status;
    uint32_t sync_in_count;
    float yaw_sigma;
    float pitch_sigma;
    float roll_sigma;
    float pos_sigma;
    float vel_sigma;
} __attribute__((packed));

ins_binary_data_struct ins_binary_data;

struct utc_time_struct
{
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
    uint16_t millisecond;
} __attribute__((packed));

struct gps_binary_data_struct
{
    uint64_t gps_time;
    float temp;
    float pres;
    utc_time_struct utc_time;
    uint64_t tow;
    uint16_t week;
    uint8_t  num_sats;
    uint8_t  fix;
    double latitude;
    double longitude;
    double altitude;
    float vel_north;
    float vel_east;
    float vel_down;
    float north_sigma;
    float east_sigma;
    float down_sigma;
    float vel_sigma;
    uint32_t time_sigma;
} __attribute__((packed));

struct gps_binary_data_struct gps_binary_data;

struct imu_binary_data_struct
{
    uint64_t gps_time;
    float q_x;
    float q_y;
    float q_z;
    float q_w;
    float rotr_x;
    float rotr_y;
    float rotr_z;
    float accel_x;
    float accel_y;
    float accel_z;
    float lin_accel_x;
    float lin_accel_y;
    float lin_accel_z;
    float YprU_yaw;
    float YprU_pitch;
    float YprU_roll;
} __attribute__((packed));

struct imu_binary_data_struct imu_binary_data;

int ins_msg = 0;
int gps_msg = 0;
int imu_msg = 0;

void publish_gps_data()
{
    gps_seq++;
    ros::Time timestamp =  ros::Time::now();

    vectornav::gps msg_gps;
    msg_gps.header.seq      = gps_seq;
    msg_gps.header.stamp    = timestamp;
    msg_gps.header.frame_id = "gps_link";

    msg_gps.UtcTime.year = gps_binary_data.utc_time.year;
    msg_gps.UtcTime.month = gps_binary_data.utc_time.month;
    msg_gps.UtcTime.day = gps_binary_data.utc_time.day;
    msg_gps.UtcTime.hour = gps_binary_data.utc_time.hour;
    msg_gps.UtcTime.minute = gps_binary_data.utc_time.minute;
    msg_gps.UtcTime.second = gps_binary_data.utc_time.second;
    msg_gps.UtcTime.millisecond = gps_binary_data.utc_time.millisecond;

    msg_gps.gps_time = (double)gps_binary_data.gps_time*1E-9;

    msg_gps.temperatur = gps_binary_data.temp;
    msg_gps.pressure = gps_binary_data.pres;

    msg_gps.Time    = (double)gps_binary_data.tow*1E-9;
    msg_gps.Week    = gps_binary_data.week;
    msg_gps.GpsFix  = gps_binary_data.fix;
    msg_gps.NumSats = gps_binary_data.num_sats;

    msg_gps.LLA.x = gps_binary_data.latitude;
    msg_gps.LLA.y = gps_binary_data.longitude;
    msg_gps.LLA.z = gps_binary_data.altitude;

    msg_gps.NedVel.x = gps_binary_data.vel_north;
    msg_gps.NedVel.y = gps_binary_data.vel_east;
    msg_gps.NedVel.z = gps_binary_data.vel_down;

    msg_gps.NedAcc.x = gps_binary_data.north_sigma;
    msg_gps.NedAcc.y = gps_binary_data.east_sigma;
    msg_gps.NedAcc.z = gps_binary_data.down_sigma;

    msg_gps.SpeedAcc = gps_binary_data.vel_sigma;
    msg_gps.TimeAcc  = gps_binary_data.time_sigma;

    pub_gps.publish(msg_gps);

    msg_std_gps.header.seq++;
    msg_std_gps.header.stamp    = timestamp;
    msg_std_gps.header.frame_id = "gps_link";

    msg_std_gps.status.status   = 0;
    msg_std_gps.status.service  = 1;

    msg_std_gps.latitude = gps_binary_data.latitude;
    msg_std_gps.longitude = gps_binary_data.longitude;
    msg_std_gps.altitude = gps_binary_data.altitude;

    msg_std_gps.position_covariance[0] = gps_binary_data.east_sigma*gps_binary_data.east_sigma;
    msg_std_gps.position_covariance[4] = gps_binary_data.north_sigma*gps_binary_data.north_sigma;
    msg_std_gps.position_covariance[8] = gps_binary_data.down_sigma*gps_binary_data.down_sigma;

    msg_std_gps.position_covariance_type = 2;

    pub_std_gps.publish(msg_std_gps);
}

void publish_ins_data()
{
    ins_seq++;
    ros::Time timestamp =  ros::Time::now();

    vectornav::ins msg_ins;
    msg_ins.header.seq      = ins_seq;
    msg_ins.header.stamp    = timestamp;
    msg_ins.header.frame_id = "ins";

    msg_ins.gps_time = (double)ins_binary_data.gps_time*1E-9;
    msg_ins.Time    = (double)gps_binary_data.tow*1E-9;
    msg_ins.Week    = gps_binary_data.week;
    msg_ins.Status  = ins_binary_data.ins_status;

    msg_ins.RPY.x = ins_binary_data.roll; // Intentional re-ordering
    msg_ins.RPY.y = ins_binary_data.pitch;
    msg_ins.RPY.z = ins_binary_data.yaw;

    msg_ins.LLA.x = ins_binary_data.latitude;
    msg_ins.LLA.y = ins_binary_data.longitude;
    msg_ins.LLA.z = ins_binary_data.altitude;

    msg_ins.NedVel.x = ins_binary_data.vel_north;
    msg_ins.NedVel.y = ins_binary_data.vel_east;
    msg_ins.NedVel.z = ins_binary_data.vel_down;

    msg_ins.RollUncertainty = ins_binary_data.roll_sigma;
    msg_ins.PitchUncertainty = ins_binary_data.pitch_sigma;
    msg_ins.YawUncertainty = ins_binary_data.yaw_sigma;
    msg_ins.PosUncertainty  = ins_binary_data.pos_sigma;
    msg_ins.VelUncertainty  = ins_binary_data.vel_sigma;

    msg_ins.SyncInTime = (double)(ins_binary_data.gps_time-ins_binary_data.sync_in_time)*1E-9;
    msg_ins.SyncInCount = ins_binary_data.sync_in_count;

    pub_ins.publish(msg_ins);
}

void publish_imu_data()
{
    imu_seq++;
    ros::Time timestamp =  ros::Time::now();

    msg_vn_imu.header.seq++;
    msg_vn_imu.header.stamp    = timestamp;
    msg_vn_imu.header.frame_id = "imu_link";
    msg_vn_imu.gps_time = (double)imu_binary_data.gps_time*1E-9;

    msg_vn_imu.orientation.x = imu_binary_data.q_x;
    msg_vn_imu.orientation.y = imu_binary_data.q_y;
    msg_vn_imu.orientation.z = imu_binary_data.q_z;
    msg_vn_imu.orientation.w = imu_binary_data.q_w;

    msg_vn_imu.YprU.x = imu_binary_data.YprU_roll;
    msg_vn_imu.YprU.y = imu_binary_data.YprU_pitch;
    msg_vn_imu.YprU.z = imu_binary_data.YprU_yaw;

    msg_vn_imu.angular_velocity.x = imu_binary_data.rotr_x;
    msg_vn_imu.angular_velocity.y = imu_binary_data.rotr_y;
    msg_vn_imu.angular_velocity.z = imu_binary_data.rotr_z;

    msg_vn_imu.acceleration.x = imu_binary_data.accel_x;
    msg_vn_imu.acceleration.y = imu_binary_data.accel_y;
    msg_vn_imu.acceleration.z = imu_binary_data.accel_z;

    msg_vn_imu.linear_acceleration.x = imu_binary_data.lin_accel_x;
    msg_vn_imu.linear_acceleration.y = imu_binary_data.lin_accel_y;
    msg_vn_imu.linear_acceleration.z = imu_binary_data.lin_accel_z;



    msg_std_imu.header.seq  += 1;
    msg_std_imu.header.stamp    = timestamp;
    msg_std_imu.header.frame_id = "imu_link";

    msg_std_imu.orientation.x = imu_binary_data.q_y; // swap x and y and negate z
    msg_std_imu.orientation.y = imu_binary_data.q_x; //needs to be fliped to go from NED to ENU
    msg_std_imu.orientation.z = -imu_binary_data.q_z; //needs to be fliped to go from NED to ENU
    msg_std_imu.orientation.w = imu_binary_data.q_w;

    msg_std_imu.orientation_covariance[0] = imu_binary_data.YprU_roll*(3.14/180)*imu_binary_data.YprU_roll*(3.14/180);
    msg_std_imu.orientation_covariance[4] = imu_binary_data.YprU_pitch*(3.14/180)*imu_binary_data.YprU_roll*(3.14/180);
    msg_std_imu.orientation_covariance[8] = imu_binary_data.YprU_yaw*(3.14/180)*imu_binary_data.YprU_roll*(3.14/180);

    msg_std_imu.angular_velocity.x = imu_binary_data.rotr_x;
    msg_std_imu.angular_velocity.y = imu_binary_data.rotr_y;
    msg_std_imu.angular_velocity.z = imu_binary_data.rotr_z;
    msg_std_imu.linear_acceleration.x = imu_binary_data.lin_accel_x;
    msg_std_imu.linear_acceleration.y = imu_binary_data.lin_accel_y;
    msg_std_imu.linear_acceleration.z = imu_binary_data.lin_accel_z;

    pub_vn_imu.publish(msg_vn_imu);
    pub_std_imu.publish(msg_std_imu);
}

void publish_sync_in()
{
    sync_in_seq++;
    ros::Time timestamp =  ros::Time::now();
    // sync_in from camera strobe
    if (pub_sync_in.getNumSubscribers() > 0)
    {
        vectornav::sync_in msg_sync_in;
        msg_sync_in.header.seq      = sync_in_seq;
        msg_sync_in.header.stamp    = timestamp;
        msg_sync_in.header.frame_id = "sync_in";
        msg_sync_in.gps_time 	    = (double)(ins_binary_data.gps_time-ins_binary_data.sync_in_time)*1E-9;
        msg_sync_in.sync_in_count   = ins_binary_data.sync_in_count;

        pub_sync_in.publish(msg_sync_in);
    }
}

void asyncBinaryResponseListener(Vn200* sender, unsigned char* data, unsigned int buf_len)
{
    int sync_byte = data[0];
    int group_number = data[1];
    int fields = data[3]*256+data[2];
    static uint32_t last_sync_in_count = 0;
    imu_resv_count++; //count up when we recieve data
    

    //ROS_INFO_STREAM(sync_byte << " " << group_number);

    msg_binary_serial.header.stamp =  ros::Time::now();
    msg_binary_serial.header.seq = msg_binary_serial.header.seq +1;

    msg_binary_serial.data.clear();
    msg_binary_serial.data.reserve(msg_binary_serial.data.size()+buf_len);
    copy(&data[0],&data[buf_len-1],back_inserter(msg_binary_serial.data));
    pub_binary_data.publish(msg_binary_serial);

    if (sync_byte == 250 && group_number == vectornav_reg_75_groups)
    {
        memcpy(&imu_binary_data, data+6, buf_len-8);
        publish_imu_data();
        if (remainder(imu_msg, 500) == 0)
        {
            imu_msg = 0;

            ROS_INFO_STREAM("IMU_Time: " << imu_binary_data.gps_time*1E-9 << " q_x: " <<
                    imu_binary_data.q_x << " q_y: " << imu_binary_data.q_y <<
                    " q_z: " <<imu_binary_data.q_z <<   " q_w: " << imu_binary_data.q_w <<
                    " rotr_x: " << imu_binary_data.rotr_x << " rotr_y: " << imu_binary_data.rotr_y <<
                    " rotr_z: " << imu_binary_data.rotr_z << " accel_x: " <<
                    imu_binary_data.accel_x << " accel_y: " << imu_binary_data.accel_y <<
                    " accel_z: " << imu_binary_data.accel_z);
        }
	      imu_msg++;
    }

    else if (sync_byte == 250 && group_number == vectornav_reg_76_groups)
    {

        memcpy(&ins_binary_data, data+8, buf_len-10);
        publish_ins_data();
        if (remainder(ins_msg, 100) == 0)
        {
            ins_msg = 0;
            ROS_INFO_STREAM("INS_Time: " << ins_binary_data.gps_time*1E-9 << " Yaw: " <<
                    ins_binary_data.yaw << " Pitch: " << ins_binary_data.pitch <<
                    " Roll: " << ins_binary_data.roll << " INS_Status: " <<
                    ins_binary_data.ins_status << " latitude: " << ins_binary_data.latitude <<
                    " longitude: " << ins_binary_data.longitude << " altitude: " << ins_binary_data.altitude);
        }
        ins_msg++;

        if (last_sync_in_count != ins_binary_data.sync_in_count)
        {
            double syncInTime = (ins_binary_data.gps_time - ins_binary_data.sync_in_time) * 1e-9;
            ROS_DEBUG_STREAM("Received strobe count:" << ins_binary_data.sync_in_count << " at GPS time "
                    << std::fixed << std::setw(12) << syncInTime);
            last_sync_in_count = ins_binary_data.sync_in_count;
            publish_sync_in();
        }

    }
    else if (sync_byte == 250 && group_number == vectornav_reg_77_groups)
    {

        memcpy(&gps_binary_data, data+8, buf_len-10);

        publish_gps_data();
        if (remainder(gps_msg, 16) == 0)
        {
            gps_msg = 0;
            ROS_INFO_STREAM("TOW: " << gps_binary_data.tow*1E-9 << " NumSats: " << (int)gps_binary_data.num_sats);
        }
        gps_msg++;
    }

}

void vnerr_msg(VN_ERROR_CODE vn_error, char* msg)
{
    switch(vn_error)
    {
        case VNERR_NO_ERROR:
            strcpy(msg, "No Error");
            break;
        case VNERR_UNKNOWN_ERROR:
            strcpy(msg, "Unknown Error");
            break;
        case VNERR_NOT_IMPLEMENTED:
            strcpy(msg, "Not implemented");
            break;
        case VNERR_TIMEOUT:
            strcpy(msg, "Timemout");
            break;
        case VNERR_INVALID_VALUE:
            strcpy(msg, "Invalid value");
            break;
        case VNERR_FILE_NOT_FOUND:
            strcpy(msg, "File not found");
            break;
        case VNERR_NOT_CONNECTED:
            strcpy(msg, "Not connected");
            break;
        default:
            strcpy(msg, "Undefined Error");
    }
}

void stop_vn200()
{
    VN_ERROR_CODE vn_retval;
    int retry_cnt = 0;


    vn_retval = vn200_setBinaryOutputRegisters(&vn200, 0, 1,  1, 1, true);

    while (vn_retval != VNERR_NO_ERROR && retry_cnt < 2)
    {
        retry_cnt++;
        vn_retval = vn200_setBinaryOutputRegisters(&vn200, 0, 1, 1, 1, true);
    }

    if (vn_retval != VNERR_NO_ERROR)
    {
        vnerr_msg(vn_retval, vn_error_msg);
        ROS_FATAL( "Could not turn off BinaryResponseListener output on device via: %s, Error Text: %s",
                port.c_str(), vn_error_msg);
        exit (EXIT_FAILURE);
    }

    ROS_INFO("Disabled binary output register");
    vn200_unregisterAsyncBinaryResponseListener(&vn200, &asyncBinaryResponseListener);
    vn200_disconnect(&vn200);
}

void mySigintHandler(int sig)
{
    g_request_shutdown = 1;
}

// Replacement "shutdown" XMLRPC callback
void shutdownCallback(XmlRpc::XmlRpcValue& params, XmlRpc::XmlRpcValue& result)
{
    int num_params = 0;

    if (params.getType() == XmlRpc::XmlRpcValue::TypeArray)
    num_params = params.size();
    if (num_params > 1)
    {
        std::string reason = params[1];
        ROS_WARN("Shutdown request received. Reason: [%s]", reason.c_str());
        g_request_shutdown = 1; // Set flag
    }

    result = ros::xmlrpc::responseInt(1, "", 0);
}

void timerCallback(const ros::TimerEvent& event)
{
	if(imu_resv_count > 0)
	{
		imu_resv_count = 0; // reset count on recieved imu messages :-)
	}else{
		
		ROS_FATAL("ERROR, IMU timed out in timerCallback");
		g_request_shutdown = 1; // Set flag
		stop_vn200();
	}
}

/////////////////////////////////////////////////
int main( int argc, char* argv[] )
{
    // Initialize ROS;
    ros::init(argc, argv, "vectornav", ros::init_options::NoSigintHandler);
    ros::NodeHandle n;
    ros::NodeHandle n_("~");

    // Read Parameters
    int baud, poll_rate_ins, poll_rate_gps, poll_rate_imu, async_output_type, async_output_rate,
        binary_data_output_port, binary_gps_data_rate, binary_ins_data_rate,
        binary_imu_data_rate;

    int retry_cnt = 0;

    VnMatrix3x3 c;
    c.c00 = 1.0;//1.0;
    c.c01 = 0.0;//0.0;
    c.c02 = 0.0;//0.0;
    c.c10 = 0.0;//0.0;
    c.c11 = 1.0;//1.0;
    c.c12 = 0.0;//0.0;
    c.c20 = 0.0;//0.0;
    c.c21 = 0.0;//0.0;
    c.c22 = 1.0;//1.0;

    // Override XMLRPC shutdown
    ros::XMLRPCManager::instance()->unbind("shutdown");
    ros::XMLRPCManager::instance()->bind("shutdown", shutdownCallback);
    signal(SIGINT, mySigintHandler);

    n_.param<std::string>("serial_port" , port     , "/dev/ttyUSB0");
    n_.param<int>(        "serial_baud" , baud     , 115200);
    n_.param<int>(        "poll_rate_gps"   , poll_rate_gps, 5);
    n_.param<int>(        "poll_rate_ins"   , poll_rate_ins, 20);
    n_.param<int>(        "poll_rate_imu"   , poll_rate_imu, 100);

    n_.param<std::string>("imu/frame_id", imu_frame_id, "LLA");
    n_.param<std::string>("gps/frame_id", gps_frame_id, "LLA");

    // Type: 0 None, 19 IMU, 20 GPS, 22 INS
    n_.param<int>(        "async_output_type"  , async_output_type, 0);
    n_.param<int>(        "async_output_rate"  , async_output_rate, 50);

    n_.param<int>(        "binary_data_output_port"  , binary_data_output_port, 1);
    n_.param<int>(        "binary_gps_data_output_rate"  , binary_gps_data_rate, 4);
    n_.param<int>(        "binary_ins_data_output_rate"  , binary_ins_data_rate, 20);
    n_.param<int>(        "binary_imu_data_output_rate"  , binary_imu_data_rate, 100);

    // Initialize Publishers
    pub_ins     = n_.advertise<vectornav::ins>    ("vn_ins", 1000);
    pub_gps     = n_.advertise<vectornav::gps>    ("vn_gps", 1000);
    //pub_sensors = n_.advertise<vectornav::sensors>("imu", 1000);
    pub_sync_in = n_.advertise<vectornav::sync_in>("vn_sync_in", 1000);
    pub_vn_imu = n_.advertise<vectornav::vectornav_imu>("vn_imu", binary_imu_data_rate);

    pub_std_imu = n_.advertise<sensor_msgs::Imu>("imu/data", binary_imu_data_rate);

    pub_std_gps = n_.advertise<sensor_msgs::NavSatFix>("gps/data", binary_gps_data_rate);

    pub_binary_data = n_.advertise<vectornav::serial_data>("binary_serial_data", 100);

    ros::Timer time_loop = n.createTimer(ros::Duration(5), timerCallback); //status timing callback each 5 seconds

    // Initialize VectorNav
    VN_ERROR_CODE vn_retval;
    ROS_INFO("Initializing vn200. Port:%s Baud:%d\n", port.c_str(), baud);

    vn_retval = vn200_connect(&vn200, port.c_str(), baud);
    if (vn_retval != VNERR_NO_ERROR)
    {
        vnerr_msg(vn_retval, vn_error_msg);
        ROS_FATAL("Could not conenct to vn200 on port:%s @ Baud:%d; Error %d \n"
                "Did you add your user to the 'dialout' group in /etc/group?",
                port.c_str(),
                baud,
                vn_retval);
        exit (EXIT_FAILURE);
    }

    usleep(10000);

    //vn_retval = vn200_setReferenceFrameRotation(&vn200,c,true);
    //ROS_INFO("vn200_setReferenceFrameRotation:::::: %d\n", vn_retval);

    //vn200_writeSettings(&vn200,true);
    
    vn200_registerAsyncBinaryResponseListener(&vn200, &asyncBinaryResponseListener);

    /* turn off asynchronous ASCII output, retry a couple of times */
    vn_retval = vn200_setAsynchronousDataOutputType(&vn200, 0, true);

    while (vn_retval != VNERR_NO_ERROR && retry_cnt < 3)
    {
        retry_cnt++;
    	vn_retval = vn200_setAsynchronousDataOutputType(&vn200, 0, true);
    }

    if (vn_retval != VNERR_NO_ERROR)
    {
        vnerr_msg(vn_retval, vn_error_msg);
        ROS_FATAL( "Could not set output type on device via: %s, Error Text: %s", port.c_str(), vn_error_msg);
        exit (EXIT_FAILURE);
    }

    ROS_INFO("About to set SynchronizationControl");

    vn_retval = vn200_setSynchronizationControl(&vn200,
         3, // SyncInMode: 3=Count number of trigger events on SYNC_IN (pin 22).
         0, // SyncInEdge: 0=rising
         0, // SyncInSkipFactor
         0, // reserved
         6, // SyncOutMode: 6= Trigger on a GPS PPS event (1 Hz) when a 3D fix is valid
         1, // SyncOutPolarity: 0=Negative 1=Positive
         0, // SyncOutSkipFactor
         100000000, // pulse width in ns
         0, true); // reserved, wait for response


    if (vn_retval != VNERR_NO_ERROR)
    {
        vnerr_msg(vn_retval, vn_error_msg);
        ROS_FATAL( "Could not set SynchronizationControl, Error Text: %s", vn_error_msg);
        exit (EXIT_FAILURE);
    } else {
        ROS_INFO("Set SynchronizationControl");
    }

    VnVector3 position;
    position.c0 = 0;
    position.c1 = -0.039;
    position.c2 = 0;

    usleep(10000);

    vn_retval = vn200_setGpsAntennaOffset(&vn200, position, true);


    if (vn_retval != VNERR_NO_ERROR)
    {
        vnerr_msg(vn_retval, vn_error_msg);
        ROS_FATAL( "Could not set GPS Antenna Offset, Error Text: %s", vn_error_msg);
        exit (EXIT_FAILURE);
    }

    usleep(10000);


    vn_retval = vn200_setBinaryOutputRegisters(&vn200, binary_data_output_port, binary_gps_data_rate,
            binary_ins_data_rate, binary_imu_data_rate, true);

    if (vn_retval != VNERR_NO_ERROR)
    {
        vnerr_msg(vn_retval, vn_error_msg);
        ROS_FATAL( "Could not set BinaryResponseListener output on device via: %s, Error Text: %s",
                port.c_str(), vn_error_msg);
        exit (EXIT_FAILURE);
    }

    ROS_INFO("DONE INIT!!!");

    while (!g_request_shutdown)
    {
        ros::spinOnce();
        usleep(500);
    }
    stop_vn200();
    usleep(100);//wait 100 ms before shutting down
    ros::shutdown();
    return 0;
}
